package org.example;

import org.junit.Test;

public class AppTest {
    private final SortingApp sortingApp = new SortingApp();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        final int[] array = null;

        sortingApp.sort(array);
    }
}
