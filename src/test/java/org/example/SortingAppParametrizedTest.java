package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppParametrizedTest {
    private int[] input;
    private int[] expectedOutput;

    private final SortingApp sortingApp = new SortingApp();

    public SortingAppParametrizedTest(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{}, new int[]{}},
                {new int[]{1}, new int[]{1}},
                {new int[]{2, 1}, new int[]{1, 2}},
                {new int[]{3, 2, 1}, new int[]{1, 2, 3}},
                {new int[]{4, 3, 2, 1}, new int[]{1, 2, 3, 4}},
                {new int[]{5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6}},
                {new int[]{7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7}},
                {new int[]{8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8}},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new int[]{11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}}
        });
    }

    @Test
    public void testSorting() {
        sortingApp.sort(input);

        assertArrayEquals(expectedOutput, input);
    }
}

