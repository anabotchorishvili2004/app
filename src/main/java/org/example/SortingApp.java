package org.example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;

public class SortingApp {
    private static final Logger logger = LoggerFactory.getLogger(SortingApp.class);

    private static final int MAX_ARGUMENTS = 10;

    public static void main(String[] args) {


        logger.info("Sorting App started.");

        if (args.length == 0) {
            logger.warn("No arguments provided.");
            return;
        }

        if (args.length > MAX_ARGUMENTS) {
            logger.warn("Too many arguments provided. Sorting up to " + MAX_ARGUMENTS + " arguments only.");
            args = Arrays.copyOf(args, MAX_ARGUMENTS);
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        SortingApp sortingApp = new SortingApp();
        sortingApp.sort(numbers);

        logger.info("Sorted numbers: " + Arrays.toString(numbers));
    }

    public void sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array must not be null.");
        }

        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
